# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from reservations.models import Building, ReservationDate, Reservation
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_protect
from django.utils.crypto import get_random_string
import datetime

def buildingAll(request):
    page = request.GET.get('page', 1)
    fromDate = request.GET.get('from')
    toDate = request.GET.get('to')

    def square(x):
        return x.building

    if fromDate and toDate:
        # Gets all available reservations date the range
        reservationDates = ReservationDate.objects.filter(
            reservation_at__range=(fromDate, toDate),
            reservation__isnull=True
        ).all()

        # Gets unique buildings
        buildings = list(set(map(square, reservationDates)))
    else:
        # Get all buildings
        buildings = Building.objects.all()

    # Paginates the results, with 10 per page
    paginator = Paginator(buildings, 10)

    try:
        buildings = paginator.page(page)
    except PageNotAnInteger:
        buildings = paginator.page(1)
    except EmptyPage:
        buildings = paginator.page(paginator.num_pages)

    return render(request, 'buildings/show.html', {
        'buildings': buildings
    })

def buildingOne(request, id):
    reservationDates = ReservationDate.objects.filter(building=id, reservation__isnull=True).all()

    # Checks if building exist
    try:
        building = Building.objects.get(id=id)
    except Building.DoesNotExist:
        return render(request, 'buildings/error.html', {
            'message': 'The resource doesn\'t exist'
        })

    return render(request, 'buildings/one.html', {
        'building': building,
        'reservationDates': reservationDates
    })


@csrf_protect
def buildingReservate(request, id):
    reservationDates = ReservationDate.objects.filter(building=id, id__in=request.POST.getlist('reservation_dates')).all()
    reservation = Reservation()

    # Checks if the sent reservation dates exist
    if len(reservationDates) < len(request.POST.getlist('reservation_dates')):
        return render(request, 'buildings/error.html', {
            'message': 'The resource doesn\'t exist'
        })

    # Checks if building exist
    try:
        building = Building.objects.get(id=id)
    except Building.DoesNotExist:
        return render(request, 'buildings/error.html', {
            'message': 'The resource doesn\'t exist'
        })

    # Inserts reservation data
    reservation.created_at = datetime.date.today()
    reservation.building = building
    reservation.code = get_random_string(length=12)
    # the total amount is: amount of reservation dates  * building rate per day
    reservation.total_amount = len(request.POST.getlist('reservation_dates')) * building.rate_day
    reservation.save()

    # Gets id of the inserted reservation
    reservationId = Reservation.objects.latest('id')

    # Updates reservation dates to reservated with the id of the reservation
    reservationDates.update(reservation=reservationId)

    return render(request, 'buildings/confirmation.html', {
        'building': building,
        'reservation': reservation,
        'reservationDates': reservationDates
    })