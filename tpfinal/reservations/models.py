# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

class City(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Building(models.Model):
    city = models.ForeignKey(City, null=False, blank=False)
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True)
    rate_day = models.FloatField(null=False, default=0.00)
    max_pax = models.IntegerField(null=False)
    image = models.ImageField(upload_to='images')
    owner = models.ForeignKey(User, null=False, blank=False)

    def __unicode__(self):
        return self.title


class Reservation(models.Model):
    building = models.ForeignKey(Building, null=False, blank=False)
    created_at = models.DateField(auto_now_add=True)
    code = models.CharField(max_length=100, null=False, blank=False)
    total_amount = models.FloatField(null=True, default=None)

    def __unicode__(self):
        return self.code


class ReservationDate(models.Model):
    building = models.ForeignKey(Building, null=False, blank=False)
    reservation = models.ForeignKey(Reservation, null=True, blank=True)
    reservation_at = models.DateField(null=False)

    def __unicode__(self):
        return str(self.reservation_at)