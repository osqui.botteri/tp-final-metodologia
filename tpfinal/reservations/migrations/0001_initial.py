# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-16 01:59
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=200, null=True)),
                ('rate_day', models.FloatField(default=0.0)),
                ('max_pax', models.IntegerField()),
                ('image', models.ImageField(upload_to='images')),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_now_add=True)),
                ('code', models.CharField(max_length=100)),
                ('total_amount', models.FloatField(default=None, null=True)),
                ('building', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reservations.Building')),
            ],
        ),
        migrations.CreateModel(
            name='ReservationDate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reservation_at', models.DateField()),
                ('building', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reservations.Building')),
                ('reservation', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='reservations.Reservation')),
            ],
        ),
        migrations.AddField(
            model_name='building',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='reservations.City'),
        ),
        migrations.AddField(
            model_name='building',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
