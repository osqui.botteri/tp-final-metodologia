from django.conf.urls import url
from reservations import views

urlpatterns = [
    url(r'^building/$', views.buildingAll, name='building.all'),
    url(r'^building/(?P<id>\d+)/reservate/$', views.buildingReservate, name='building.reservate'),
    url(r'^building/(?P<id>\d+)/$', views.buildingOne, name='building.one'),
]