# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

# Register your models here.
from .models import Building
from .models import City
from .models import Reservation
from .models import ReservationDate

admin.site.register(City)
admin.site.register(Reservation)


class ReservationDateInline(admin.TabularInline):
    model = ReservationDate
    fk_name = 'building'
    max_num = 7


class AdminBuilding(admin.ModelAdmin):
    inlines = [ReservationDateInline]


admin.site.register(Building, AdminBuilding)